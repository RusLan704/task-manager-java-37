package ru.bakhtiyarov.tm.listener.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataBinaryClearListener extends AbstractListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Clear bin file.";
    }

    @EventListener(condition = "@dataBinaryClearListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA BIN CLEAR]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.clearBinary(session);
        System.out.println("[OK]");
    }

}


