package ru.bakhtiyarov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.UserDTO;
import ru.bakhtiyarov.tm.endpoint.UserEndpoint;

@Component
public final class UserViewProfileListener extends AbstractListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_VIEW_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile.";
    }

    @EventListener(condition = "@userViewProfileListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW USER]");
        @NotNull SessionDTO session = sessionService.getSession();
        UserDTO user = userEndpoint.findUserById(session);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        @NotNull final String email = user.getEmail();
        if (!email.isEmpty()) System.out.println("EMAIL: " + email);
        @NotNull final String firstName = user.getFirstName();
        if (!firstName.isEmpty()) System.out.println("FIRST NAME: " + firstName);
        @NotNull final String lastName = user.getLastName();
        if (!lastName.isEmpty()) System.out.println("LAST NAME: " + lastName);
        @NotNull final String middleName = user.getMiddleName();
        if (!middleName.isEmpty()) System.out.println("MIDDLE NAME: " + middleName);
        System.out.println("[OK]");
    }

}