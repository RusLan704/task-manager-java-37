package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.exception.empty.UnknownSessionException;
import ru.bakhtiyarov.tm.exception.user.SessionNotFoundException;

@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO session;

    @NotNull
    @Override
    public SessionDTO getSession() {
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @Override
    public void setSession(@Nullable final SessionDTO session) {
        if (session == null) throw new UnknownSessionException();
        this.session = session;
    }

    @Override
    public void clearSession() {
        session = null;
    }

}
