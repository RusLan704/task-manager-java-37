package ru.bakhtiyarov.tm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public final class DomainDTO implements Serializable {

    @NotNull
    private List<Project> project = new ArrayList<>();

    @NotNull
    private List<Task> task = new ArrayList<>();

    @NotNull
    private List<User> user = new ArrayList<>();

    @NotNull
    private List<Session> sessions = new ArrayList<>();

}
