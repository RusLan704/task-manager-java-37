package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.repository.ISessionRepository;
import ru.bakhtiyarov.tm.util.HashUtil;
import ru.bakhtiyarov.tm.util.SignatureUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionService extends AbstractService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    @Autowired
    public SessionService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionRepository sessionRepository
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session open(@Nullable String login, @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sign(session);
        session.setUser(user);
        return save(session);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null) return false;
        if (passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return sessionRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session sign(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null) throw new AccessDeniedException();
        session.setSignature(signature);
        return session;
    }

    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @SneakyThrows
    public void validate(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String userId = user.getId();
        sessionRepository.deleteAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void signOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull ISessionRepository userRepository = getRepository();
        userRepository.deleteAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void close(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @NotNull ISessionRepository userRepository = getRepository();
        userRepository.delete(session);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void closeAll(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        sessionRepository.deleteAllByUserId(session.getUserId());
    }

    @Override
    @Transactional
    public void removeAll() {
         sessionRepository.deleteAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable Session session) {
        validate(session);
        return sessionRepository.findAllByUserId(session.getUserId());
    }

}
