package ru.bakhtiyarov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;

import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @Nullable
    UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @Nullable
    UserDTO unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    void removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    void removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @NotNull
    @SneakyThrows
    List<UserDTO> findAllUsers(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

}
