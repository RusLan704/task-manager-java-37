package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    UserDTO createUserByLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @Nullable
    UserDTO createUserByLoginPasswordEmail(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    );

    @Nullable
    UserDTO createUserByLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role") final Role role
    );

    @Nullable
    UserDTO findUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    UserDTO findUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @Nullable
    UserDTO updateUserPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @Nullable
    UserDTO updateUserEmail(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "email", partName = "email") final String password
    );

    @Nullable
    UserDTO updateUserFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "firstName", partName = "firstName") final String firstName
    );

    @Nullable
    UserDTO updateUserLastName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "lastName", partName = "lastName") final String lastName
    );

    @Nullable
    UserDTO updateUserMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "middleName", partName = "middleName") final String middleName
    );

    @Nullable
    UserDTO updateUserLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

}
