package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName
    );

    @WebMethod
    void createTaskByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @WebMethod
    void clearAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @WebMethod
    @NotNull
    void removeAllByUserIdAndProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectId
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findAllByUserIdAndProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    void removeTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    void removeTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    void removeTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @Nullable
    @WebMethod
    TaskDTO updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

}
