package ru.bakhtiyarov.tm.service;

import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class ProjectDTOServiceTest {

 /*   @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void testCreateUserIdName() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create("1", "name");
        Assert.assertEquals(1, projectService.findAll().size());
        ProjectDTO projectDto = projectService.findOneByName("1", "name");
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectDto.getName(), "name");
        Assert.assertEquals(projectDto.getUserId(), "1");
    }

    @Test
    public void testCreateUserIdNameDescription() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create("1", "name", "234");
        Assert.assertEquals(1, projectService.findAll().size());
        ProjectDTO projectDto = projectService.findOneByName("1", "name");
        Assert.assertNotNull(projectDto);
        Assert.assertEquals("name", projectDto.getName());
        Assert.assertEquals("234", projectDto.getDescription());
        Assert.assertEquals("1", projectDto.getUserId());
    }

    @Test
    public void testAdd() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        ProjectDTO projectDto = new ProjectDTO("1234", "2");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        ProjectDTO projectDto = new ProjectDTO("1234", "2");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.remove(projectDto.getUserId(), projectDto);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void testFindAll() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        ProjectDTO projectDto = new ProjectDTO("1234", "2");
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(4, projectService.findAll(projectDto.getUserId()).size());
    }

    @Test
    public void clearAll() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        ProjectDTO projectDto = new ProjectDTO("1234", "2");
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(4, projectService.findAll(projectDto.getUserId()).size());
        projectService.clearAll();
        Assert.assertTrue(projectService.findAll(projectDto.getUserId()).isEmpty());
    }

    @Test
    public void testFindOneByName() {
        ProjectDTO projectDto = new ProjectDTO("project", "2231");
        projectService.add(projectDto.getUserId(), projectDto);
        ProjectDTO tempProjectDTO = projectService.findOneByName(projectDto.getUserId(), projectDto.getName());
        Assert.assertNotNull(tempProjectDTO);
        Assert.assertEquals(projectDto.getId(), tempProjectDTO.getId());
    }

    @Test
    public void testFindOneByIndex() {
        ProjectDTO projectDto = new ProjectDTO("project", "2231");
        projectService.add(projectDto.getUserId(), projectDto);
        ProjectDTO tempProjectDTO = projectService.findOneByIndex(projectDto.getUserId(), 0);
        Assert.assertNotNull(tempProjectDTO);
        Assert.assertEquals(projectDto.getId(), tempProjectDTO.getId());
    }

    @Test
    public void testFindOneById() {
        ProjectDTO projectDto = new ProjectDTO("project", "2231");
        projectService.add(projectDto.getUserId(), projectDto);
        ProjectDTO tempProjectDTO = projectService.findOneById(projectDto.getUserId(), projectDto.getId());
        Assert.assertNotNull(tempProjectDTO);
        Assert.assertEquals(projectDto.getId(), tempProjectDTO.getId());
    }

    @Test
    public void testRemoveOneByIndex() {
        ProjectDTO projectDto = new ProjectDTO("project", "2231");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll(projectDto.getUserId()).size());
        projectService.removeOneByIndex(projectDto.getUserId(), 0);
        Assert.assertEquals(0, projectService.findAll(projectDto.getUserId()).size());
    }

    @Test
    public void testRemoveOneById() {
        ProjectDTO projectDto = new ProjectDTO("project", "2231");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll(projectDto.getUserId()).size());
        projectService.removeOneById(projectDto.getUserId(), projectDto.getId());
        Assert.assertEquals(0, projectService.findAll(projectDto.getUserId()).size());
    }

    @Test
    public void testRemoveOneByName() {
        ProjectDTO projectDto = new ProjectDTO("project", "2231");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll(projectDto.getUserId()).size());
        projectService.removeOneByName(projectDto.getUserId(), projectDto.getName());
        Assert.assertEquals(0, projectService.findAll(projectDto.getUserId()).size());
    }

    @Test
    public void testUpdateTakByIndex() {
        ProjectDTO projectDto = new ProjectDTO("name", "description", "123");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll(projectDto.getUserId()).size());
        ProjectDTO tempProjectDTO = projectService.updateProjectByIndex(projectDto.getUserId(), 0, "new name", "new description");
        Assert.assertNotNull(tempProjectDTO);
        Assert.assertEquals(tempProjectDTO.getId(), projectDto.getId());
        Assert.assertEquals(tempProjectDTO.getName(), "new name");
        Assert.assertEquals(tempProjectDTO.getDescription(), "new description");
    }

    @Test
    public void testUpdateProjectById() {
        ProjectDTO projectDto = new ProjectDTO("name", "description", "123");
        projectService.add(projectDto.getUserId(), projectDto);
        Assert.assertEquals(1, projectService.findAll(projectDto.getUserId()).size());
        ProjectDTO tempProjectDTO = projectService.updateProjectById(projectDto.getUserId(), projectDto.getId(), "new name", "new description");
        Assert.assertNotNull(tempProjectDTO);
        Assert.assertEquals(tempProjectDTO.getId(), projectDto.getId());
        Assert.assertEquals(tempProjectDTO.getName(), "new name");
        Assert.assertEquals(tempProjectDTO.getDescription(), "new description");
        ProjectDTO nullProjectDTO = projectService.updateProjectById("1234", projectDto.getId(), "new name", "new description");
        Assert.assertNull(nullProjectDTO);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithEmptyUserId() {
        projectService.create("", "name");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithNullUserId() {
        projectService.create(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithEmptyName() {
        projectService.create("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithNullName() {
        projectService.create("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithEmptyUserId() {
        projectService.create("", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithNullUserId() {
        projectService.create(null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithEmptyName() {
        projectService.create("123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithNullName() {
        projectService.create("123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithEmptyDesc() {
        projectService.create("123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithNullDesc() {
        projectService.create("123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithEmptyUserId() {
        projectService.add("", new ProjectDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithNullUserId() {
        projectService.add(null, new ProjectDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithEmptyUserId() {
        projectService.remove("", new ProjectDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithNullUserId() {
        projectService.remove(null, new ProjectDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithEmptyUserId() {
        projectService.findAll("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithNullUserId() {
        projectService.findAll(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithEmptyUserId() {
        projectService.clear("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithNullUserId() {
        projectService.clear(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithEmptyUserId() {
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithNullUserId() {
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithMinusIndex() {
        projectService.removeOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithNullIndex() {
        projectService.removeOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithIncorrectIndex() {
        ProjectDTO projectDto = new ProjectDTO("name", "123");
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.removeOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyUserId() {
        projectService.removeOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithNullUserId() {
        projectService.removeOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyId() {
        projectService.removeOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithNullId() {
        projectService.removeOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneNameIdWithEmptyUserId() {
        projectService.removeOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithNullUserId() {
        projectService.removeOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneNameIdWithEmptyName() {
        projectService.removeOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithNullName() {
        projectService.removeOneByName("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyUserId() {
        projectService.updateProjectById("", "123", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIdWithNullUserId() {
        projectService.updateProjectById(null, "123", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyId() {
        projectService.updateProjectById("123", "", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateProjectByIdWithNullId() {
        projectService.updateProjectById("123", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyName() {
        projectService.updateProjectById("123", "123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIdWithNullName() {
        projectService.updateProjectById("123", "123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyDesc() {
        projectService.updateProjectById("123", "123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIdWithNullDesc() {
        projectService.updateProjectById("123", "123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIndexIdWithEmptyUserId() {
        projectService.updateProjectByIndex("", 0, "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIndexWithNullUserId() {
        projectService.updateProjectByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexIdWithMinusId() {
        projectService.updateProjectByIndex("123", -10, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexWithNullId() {
        projectService.updateProjectByIndex("123", null, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexWithIncorrectId() {
        ProjectDTO projectDto = new ProjectDTO("name", "123");
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.updateProjectByIndex("123", 2, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIndexIdWithEmptyName() {
        projectService.updateProjectByIndex("123", 0, "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIndexWithNullName() {
        projectService.updateProjectByIndex("123", 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIndexIdWithEmptyDesc() {
        projectService.updateProjectByIndex("123", 0, "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIndexWithNullDesc() {
        projectService.updateProjectByIndex("123", 0, "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithEmptyUserId() {
        projectService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithNullUserId() {
        projectService.findOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithEmptyId() {
        projectService.findOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithNullId() {
        projectService.findOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithEmptyUserId() {
        projectService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithNullUserId() {
        projectService.findOneById(null, "123");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithMinusUserIndex() {
        projectService.findOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithNullIndex() {
        projectService.findOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithIncorrectIndex() {
        ProjectDTO projectDto = new ProjectDTO("name", "123");
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.add(projectDto.getUserId(), projectDto);
        projectService.findOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithEmptyUserId() {
        projectService.findOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameIdWithNullUserId() {
        projectService.findOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithEmptyName() {
        projectService.findOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameIdWithNullName() {
        projectService.findOneByName("123", null);
    }
*/
}
