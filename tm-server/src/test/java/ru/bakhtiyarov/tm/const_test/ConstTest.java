package ru.bakhtiyarov.tm.const_test;

import org.jetbrains.annotations.NotNull;

public interface ConstTest {

    @NotNull
    String FALSE_USER_ID = "false id";

    @NotNull
    String FALSE_NAME = "false name";

    @NotNull
    String FALSE_ID = "false id";

    @NotNull
    String FALSE_LOGIN = "false login";

    @NotNull
    String FALSE_PASSWORD = "false password";

}
