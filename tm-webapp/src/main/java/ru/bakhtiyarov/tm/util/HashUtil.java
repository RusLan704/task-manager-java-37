package ru.bakhtiyarov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull
    String SECRET = "12312341241231";

    @NotNull
    Integer ITERATION = 3412;

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @NotNull
    @SneakyThrows
    static String md5(@NotNull final String value) {
        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        final byte[] array = md.digest(value.getBytes());
        @NotNull final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
        }
        return sb.toString();
    }

}
